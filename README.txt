
INSTALL
-------

1. Download and install cronplus.
   http://drupal.org/project/cronplus
2. Install logwatcher.
3. Setup specific reports you would like to receive at
   Administer > Logs > Log Watcher
4. Grant 'administer logwatcher' permission to each role
   you would like to receive the reports by email at:
   Administer > User management > Access control

BUGS/FEATURE REQUESTS
---------------------

http://drupal.org/project/issues/logwatcher
